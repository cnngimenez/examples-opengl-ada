-- camera.adb --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Numerics.Generic_Elementary_Functions;
with Ada.Text_Io;
use Ada.Text_Io;

package body Camera is
   Zero_Matrix4 : constant GL.Types.Singles.Matrix4 :=
     (others => (others => 0.0));

   
   package Single_Math_Functions is new
     Ada.Numerics.Generic_Elementary_Functions (GL.Types.Single);

   
   use type Gl.Types.Singles.Vector3; -- add operations for Vector3 
   
   Radians_Per_Degree : constant Radian := Ada.Numerics.Pi / 180.0;

   function Radians (Angle : Degree) return Radian is
   begin
      return Radian (Angle) * Radians_Per_Degree;
   end Radians;

   
   function Length (V : GL.Types.Singles.Vector3) return GL.Types.Single is
      use Single_Math_Functions;
      use GL;
   begin
      return Sqrt (V (X) * V (X) + V (Y) * V (Y) + V (Z) * V (Z));
   end Length;

   
   function Normalized (V : Singles.Vector3) return Singles.Vector3 is
      use GL;
      L : constant Single := Length (V);
   begin
      return (V (X) / L, V (Y) / L, V (Z) / L);
   end Normalized;
   
      --  ------------------------------------------------------------------------
   --  Perspective_Matrix is derived from Computer Graphics Using OpenGL
   --  Chapter 7, transpose of equation 7.13
   function Perspective_Matrix (Top, Bottom, Left, Right, Near, Far : Single)
                                 return GL.Types.Singles.Matrix4 is
      use GL;
      dX         : constant Single := Right - Left;
      dY         : constant Single := Top - Bottom;
      dZ         : constant Single := Far - Near;
      Matrix     : GL.Types.Singles.Matrix4 := Zero_Matrix4;
   begin
      Matrix (X, X) := 2.0 * Near / dX;
      Matrix (Z, X) := (Right + Left) / dX;
      Matrix (Y, Y) := 2.0 * Near / dY;
      Matrix (Z, Y) :=  (Top + Bottom) / dY;
      Matrix (Z, Z) := -(Far + Near) / dZ;
      Matrix (W, Z) := -2.0 * Far * Near / dZ;
      Matrix (Z, W) := -1.0;
      return Matrix;
   end Perspective_Matrix;

   
   --  ------------------------------------------------------------------------
   --  Perspective_Matrix is derived from Computer Graphics Using OpenGL
   --  Chapter 7, top, bottom, left and right equations following equation 7.13
   function Perspective_Matrix (View_Angle : Degree; Aspect, Near, Far : Single)
			       return GL.Types.Singles.Matrix4 is
      use Single_Math_Functions;

      Top    : Single;
      Bottom : Single;
      Right  : Single;
      Left   : Single;
   begin
      Top := Near * Tan (Single (0.5 * Radians (View_Angle)));
      Bottom := -Top;
      Right  := Top * Aspect;
      Left   := -Right;
      return Perspective_Matrix (Top, Bottom, Left, Right, Near, Far);
   end Perspective_Matrix;

   
   --  ------------------------------------------------------------------------
   --  Init_Lookat_Transform is derived from
   --  Computer Graphics Using OpenGL, Chapter 7, transpose of equation 7.2
   --  except, (W, W) = 1 (not 0)
   procedure Init_Lookat_Transform
     (Position, Target, Up : Singles.Vector3;
      Look_At              : out GL.Types.Singles.Matrix4) is
      use GL;
      --  Reference co-ordinate frame (u, v, n)
      --  Forward (n): camera axis
      --  Side (u): axis through side of camera, perpendicular to Forward
      --  Up_New (v): vertical axis of camera, perpendicular to Forward and Side
      Forward : Singles.Vector3 := Position - Target; --  n
      Side    : Singles.Vector3
	:= GL.Types.Singles.Cross_Product (Up, Forward);       --  u = Up x n
      Up_New  : Singles.Vector3
	:= GL.Types.Singles.Cross_Product (Forward, Side);     --  v = n x u
   begin
      Forward := Normalized (Forward);          --  n / |n|
      Side := Normalized (Side);                --  u / |u|
      Up_New := Normalized (Up_New);            --  v / |v|

      Look_At := GL.Types.Singles.Identity4;
      Look_At (X, X) := Side (X);     --  u.x
      Look_At (Y, X) := Side (Y);     --  u.y
      Look_At (Z, X) := Side (Z);     --  u.z

      Look_At (X, Y) := Up_New (X);   --  v.x
      Look_At (Y, Y) := Up_New (Y);   --  v.y
      Look_At (Z, Y) := Up_New (Z);   --  v.z

      Look_At (X, Z) := Forward (X);  --  n.x;
      Look_At (Y, Z) := Forward (Y);  --  n.y
      Look_At (Z, Z) := Forward (Z);  --  n.z

      Look_At (W, X) := -GL.Types.Singles.Dot_Product (Position, Side);
      Look_At (W, Y) := -GL.Types.Singles.Dot_Product (Position, Up_New);
      Look_At (W, Z) := -GL.Types.Singles.Dot_Product (Position, Forward);
   end Init_Lookat_Transform;
   
   procedure Init_Perspective_Transform (View_Angle : Degree;
					 Width, Height, Z_Near, Z_Far : Single;
					 Transform  : out GL.Types.Singles.Matrix4) is
   begin
      Transform := Perspective_Matrix (View_Angle, Width / Height,
				      Z_Near, Z_Far);
   end Init_Perspective_Transform;
   
   procedure Print_Matrix (Name    : String;
                           aMatrix : GL.Types.Singles.Matrix3) is
   begin
      Put_Line (Name & ":");
      for Row in GL.Index_3D'Range loop
         for Column in GL.Index_3D'Range loop
            Put (GL.Types.Single'Image (aMatrix (Row, Column)) & "   ");
         end loop;
         New_Line;
      end loop;
      New_Line;
   end Print_Matrix;
   
   procedure Print_Matrix (Name    : String;
                           aMatrix : GL.Types.Singles.Matrix4) is
   begin
      Put_Line (Name & ":");
      for Row in GL.Index_Homogeneous'Range loop
         for Column in GL.Index_Homogeneous'Range loop
            Put (GL.Types.Single'Image (aMatrix (Row, Column)) & "   ");
         end loop;
         New_Line;
      end loop;
      New_Line;
   end Print_Matrix;

   
   procedure Set_MVP_Matrix (Render_Program : GL.Objects.Programs.Program) is
      use type GL.Types.Singles.Matrix4;
      Model_Matrix      : constant GL.Types.Singles.Matrix4 := Singles.Identity4;
      Projection_Matrix : GL.Types.Singles.Matrix4 := Singles.Identity4;
      View_Matrix       : GL.Types.Singles.Matrix4 := Singles.Identity4;
   begin
      MVP_Location := GL.Objects.Programs.Uniform_Location(Render_Program,
							  "MVP");

      Init_Perspective_Transform (View_Angle,
				  View_Width, View_Height,
				  Z_Near, Z_Far,
				  Projection_Matrix);
      Init_Lookat_Transform (Camera_Position, Look_At, Up, View_Matrix);
      MVP_Matrix := Projection_Matrix * View_Matrix * Model_Matrix;
      
      --  Print_Matrix("Model Matrix", Model_Matrix);
      --  Print_Matrix("View Matrix", View_Matrix);
      --  Print_Matrix("Projection Matrix", Projection_Matrix);

      --  Print_Matrix ("MVP Matrix", MVP_Matrix);
   exception
      when others =>
	 Put_Line ("An exception occurred in Set_MVP_Matrix.");
	 raise;
   end Set_MVP_Matrix;
   
   
   procedure Update_Cam is
      use Single_Math_Functions;   
   begin
      Camera_Position(Gl.X) := Cos(Single(Radians(Angle_X))) * Distance;
      Camera_Position(Gl.Y) := Sin(Single(Radians(Angle_Y))) * Distance;
   end Update_Cam;
   
   procedure Move_Up is 
   begin
      if Angle_Y < 360.0 then
	 Angle_Y := Angle_Y + 1.0;
      end if;
      
      Update_Cam;
   end Move_Up;
   
   procedure Move_Down is 
   begin
      if Angle_Y > 0.0 then
	 Angle_Y := Angle_Y - 1.0;
      end if;

      Update_Cam;      
   end Move_Down;
   
   procedure Move_Left is 
   begin
      if Angle_X > 0.0 then
	 Angle_X := Angle_X - 1.0;
      end if;

      Update_Cam;      
   end Move_Left;
   
   procedure Move_Right is 
   begin
      if Angle_X < 360.0 then
	 Angle_X := Angle_X + 1.0;
      end if;

      Update_Cam;      
   end Move_Right;
   
   procedure Move_Farer is 
   begin
      if Distance < 100.0 then
	 Distance := Distance + 1.0;
      end if;

      Update_Cam;      
   end Move_Farer;
   
   procedure Move_Nearer is 
   begin
      if Distance > 0.0 then
	 Distance := Distance - 1.0;
      end if;

      Update_Cam;      
   end Move_Nearer;
   
   
end Camera;
