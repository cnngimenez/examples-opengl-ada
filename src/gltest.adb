-- gltest.adb --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------


-- Examples obtained from a C example here
-- http://www.opengl-tutorial.org/beginners-tutorials/tutorial-1-opening-a-window/ 
--
-- and here:
-- http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/
--
-- Without the shader part.
--
-- Some part was taken from the very_simple example in Ada's OpenGLAda
-- library.
--
-- This example is not proper, because the setup and render part is repeated
-- every time. This can be avoided creating the buffer first instead of 
-- recreating each time the loop starts.

with Gl.Buffers;
with Gl.Objects;
with Gl.Objects.Buffers;
with Gl.Objects.Vertex_Arrays;
with Gl.Types;
with Gl.Attributes;

with Glfw.Windows;
with Glfw.Windows.Context;
with Glfw.Input;
with Glfw.Input.Keys;
with Glfw;

procedure Gltest is
   
   procedure Render is
      Vertex_Array : Gl.Objects.Vertex_Arrays.Vertex_Array_Object;
      Vertex_Buffer : Gl.Objects.Buffers.Buffer;
      
      procedure Load_Vertex_Buffer is new Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector3_Pointers);
      
      use Gl.Types; -- Singles is here. Also operators overloading of this type.
      use Gl.Objects.Buffers; -- Array_Buffer is here.
      
      Vertices : constant Singles.Vector3_Array (1..2) :=
	((0.0, 0.5, 0.5),
	 (-0.5, -0.5, -0.5));
      
   begin
      Vertex_Array.Initialize_Id;
      Vertex_Array.Bind;
      
      Vertex_Buffer.Initialize_Id;
      Array_Buffer.Bind(Vertex_Buffer);
      Load_Vertex_Buffer(Array_Buffer,
			 Vertices,
			 Static_Draw);
     
     
      Gl.Attributes.Enable_Vertex_Attrib_Array(0);
      Gl.Attributes.Set_Vertex_Attrib_Pointer(0, 3, 
					      Gl.Types.Single_Type, False,
					      0, 0);
      
      
      
      -- glDrawArrays
      Gl.Objects.Vertex_Arrays.Draw_Arrays(Gl.Types.Triangles, 0, 3);
      
      Gl.Attributes.Disable_Vertex_Attrib_Array(0);
   end Render;
   
   
   
   use Glfw.Input; -- Needed for key_state() below
   
   Main_Window : aliased Glfw.Windows.Window;
   Title : constant String := "Test";
   Running : Boolean := True;
begin
   
   -- GLFWwindow* window;

   -- /* Initialize the library */
   --  if (!glfwInit())
   --      return -1;
   Glfw.Init;
   
   --  /* Create a windowed mode window and its OpenGL context */
   --  window = glfwCreateWindow(640, 480, "Hello World", NULL, NULL);
   Main_Window.Init(640, 480, Title);
   -- glfwCreateWindow(int width, int height, char* title, monitor, share)
   
   --  if (!window)
   --  {
   --      glfwTerminate();
   --      return -1;
   --  }

   --  /* Make the window's context current */
   --  glfwMakeContextCurrent(window);
   Glfw.Windows.Context.Make_Current(Main_Window'Access);
   
   --  /* Loop until the user closes the window */
   --  while (!glfwWindowShouldClose(window))
   --  {
   --      /* Render here */
   --      glClear(GL_COLOR_BUFFER_BIT);

   --      /* Swap front and back buffers */
   --      glfwSwapBuffers(window);

   --      /* Poll for and process events */
   --      glfwPollEvents();
   --  }
   
   
      
   while Running loop
      declare
	 Clearv : Gl.Buffers.Buffer_Bits;
      begin
	 Clearv.Color := True;
	 Gl.Buffers.Clear(Clearv);
      end;
      
      Render;
      
      Glfw.Windows.Context.Swap_Buffers(Main_Window'Access);
      
      Glfw.Input.Poll_Events;
      
      Running := Running and not
	(Main_Window.Key_State(Glfw.Input.Keys.Escape) = Glfw.Input.Pressed);
      Running := Running and not Main_Window.Should_Close;
   end loop;
   
   
   -- glfwTerminate();
   Glfw.shutdown;
   
end Gltest;
