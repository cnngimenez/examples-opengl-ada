-- gltest_with_shaders.adb --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

-- See http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/

with Gl.Buffers;
with Gl.Objects;
with Gl.Objects.Buffers;
with Gl.Objects.Vertex_Arrays;
with Gl.Types;
with Gl.Attributes;

-- shaders
with Gl.Objects.Programs;
with Gl.Objects.Shaders;
with Gl.Files;

with Glfw.Windows;
with Glfw.Windows.Context;
with Glfw.Input;
with Glfw.Input.Keys;
with Glfw;

with Ada.Text_Io; 
use Ada.Text_Io;

procedure Gltest_With_Shaders is
   
    -- Created with load_shaders():
   Rendering_Program : Gl.Objects.Programs.Program;
   
   function Load_Shaders(Vertex_Path:String; Fragment_Path:String) 
			return Gl.Objects.Programs.Program is
      
      Shader_Program : Gl.Objects.Programs.Program;
      My_Fshader : Gl.Objects.Shaders.Shader(Gl.Objects.Shaders.Fragment_Shader);
      My_Vshader : Gl.Objects.Shaders.Shader(Gl.Objects.Shaders.Vertex_Shader);
   begin
      --  // Create the shaders
      --  GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
      
      My_Vshader.Initialize_Id;
      
      --  GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
      
      My_Fshader.Initialize_Id;
      
      --  // Read the Vertex Shader code from the file
      --  std::string VertexShaderCode;
      --  std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
      --  if(VertexShaderStream.is_open()){
      --  	std::stringstream sstr;
      --  	sstr << VertexShaderStream.rdbuf();
      --  	VertexShaderCode = sstr.str();
      --  	VertexShaderStream.close();
      --  }else{
      --  	printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
      --  	getchar();
      --  	return 0;
      --  }
      
      Gl.Files.Load_Shader_Source_From_File(My_Vshader, Vertex_Path); 

      --  // Read the Fragment Shader code from the file
      --  std::string FragmentShaderCode;
      --  std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
      --  if(FragmentShaderStream.is_open()){
      --  	std::stringstream sstr;
      --  	sstr << FragmentShaderStream.rdbuf();
      --  	FragmentShaderCode = sstr.str();
      --  	FragmentShaderStream.close();
      --  }
      
      Gl.Files.Load_Shader_Source_From_File(My_Fshader, Fragment_Path);
      
      --  GLint Result = GL_FALSE;
      --  int InfoLogLength;

      --  // Compile Vertex Shader
      --  printf("Compiling shader : %s\n", vertex_file_path);
      --  char const * VertexSourcePointer = VertexShaderCode.c_str();
      --  glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
      --  glCompileShader(VertexShaderID);
      
      My_Vshader.Compile;

      --  // Check Vertex Shader
      --  glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
      --  glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
      --  if ( InfoLogLength > 0 ){
      --  	std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
      --  	glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
      --  	printf("%s\n", &VertexShaderErrorMessage[0]);
      --  }
      
      if not My_Vshader.Compile_Status then
	 Put_Line("Vertex Shader compilation failed.");
	 Put_Line("Log:");
	 Put_Line(My_Vshader.Info_Log);
      end if;
      
      --  // Compile Fragment Shader
      --  printf("Compiling shader : %s\n", fragment_file_path);
      --  char const * FragmentSourcePointer = FragmentShaderCode.c_str();
      --  glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
      --  glCompileShader(FragmentShaderID);
      
      My_Fshader.Compile;
      
      --  // Check Fragment Shader
      --  glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
      --  glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
      --  if ( InfoLogLength > 0 ){
      --  	std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
      --  	glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
      --  	printf("%s\n", &FragmentShaderErrorMessage[0]);
      --  }
      
      if not My_Fshader.Compile_Status then
	 Put_Line("Fragment Shader compilation failed.");
	 Put_Line("log:");
	 Put_Line(My_FShader.Info_Log);
      end if;
      
      --  // Link the program
      --  printf("Linking program\n");
      --  GLuint ProgramID = glCreateProgram();
      --  glAttachShader(ProgramID, VertexShaderID);
      --  glAttachShader(ProgramID, FragmentShaderID);
      --  glLinkProgram(ProgramID);
      
      Shader_Program.Initialize_Id;
      Shader_Program.Attach(My_Vshader);
      Shader_Program.Attach(My_Fshader);
      Shader_Program.Link;
           
      --  // Check the program
      --  glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
      --  glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
      --  if ( InfoLogLength > 0 ){
      --  	std::vector<char> ProgramErrorMessage(InfoLogLength+1);
      --  	glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
      --  	printf("%s\n", &ProgramErrorMessage[0]);
      --  }
      if not Shader_Program.Link_Status then
	 Put_Line("Linking: failed.");
	 Put_Line("Log:");
	 Put_Line(Gl.Objects.Programs.Info_Log(Shader_Program));
      end if;
      
      
      --  glDetachShader(ProgramID, VertexShaderID);
      --  glDetachShader(ProgramID, FragmentShaderID);
      
      --  glDeleteShader(VertexShaderID);
      --  glDeleteShader(FragmentShaderID);
      
      Gl.Objects.Shaders.Release_Shader_Compiler;
      
      --  return ProgramID;
      
      return Shader_Program;
   end Load_Shaders;
   
   procedure Setup is
      Vertex_Array : Gl.Objects.Vertex_Arrays.Vertex_Array_Object;
      Vertex_Buffer : Gl.Objects.Buffers.Buffer;
      
      procedure Load_Vertex_Buffer is new Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector3_Pointers);
      
      use Gl.Types; -- Singles is here. Also operators overloading of this type.
      use Gl.Objects.Buffers; -- Array_Buffer is here.
      
      Vertices : constant Singles.Vector3_Array (1..2) :=
	((0.0, 0.5, 0.5),
	 (-0.5, -0.5, -0.5));
      
      Position_Location : Gl.Attributes.Attribute;
   begin
      Vertex_Array.Initialize_Id;
      Vertex_Array.Bind;
      
      Vertex_Buffer.Initialize_Id;
      Array_Buffer.Bind(Vertex_Buffer);
      Load_Vertex_Buffer(Array_Buffer,
			 Vertices,
			 Static_Draw);
      
      Rendering_Program := Load_Shaders("./shaders/vertex_shader.glsl",
				       "./shaders/fragment_shader.glsl");
      
      Position_Location := Gl.Objects.Programs.Attrib_Location(Rendering_Program,
							      "position");
      
      Gl.Attributes.Set_Vertex_Attrib_Pointer(Position_Location, 3, 
					      Gl.Types.Single_Type, False,
					      0, 0);
      Gl.Attributes.Enable_Vertex_Attrib_Array(Position_Location);
      
   end Setup;
   
   
   procedure Render is
   begin
      Rendering_Program.Use_Program;
      Gl.Objects.Vertex_Arrays.Draw_Arrays(Gl.Types.Triangles, 0 ,3);
   end Render;
   
   
   
   use Glfw.Input; -- Needed for key_state() below
   
   Main_Window : aliased Glfw.Windows.Window;
   Title : constant String := "Test";
   Running : Boolean := True;
begin
   
   Glfw.Init;
   
   Main_Window.Init(640, 480, Title);
   Glfw.Windows.Context.Make_Current(Main_Window'Access);
   

   Setup;   
   
   while Running loop   
      declare
	 Clearv : Gl.Buffers.Buffer_Bits;
      begin
	 Clearv.Color := True;
	 Clearv.Depth := True;
	 Gl.Buffers.Clear(Clearv);
      end;
            
      Render;
      
      Glfw.Windows.Context.Swap_Buffers(Main_Window'Access);      
      Glfw.Input.Poll_Events;      
      Running := Running and not
	(Main_Window.Key_State(Glfw.Input.Keys.Escape) = Glfw.Input.Pressed);
      Running := Running and not Main_Window.Should_Close;
   end loop;

   Glfw.shutdown;   
   
end Gltest_With_Shaders;
