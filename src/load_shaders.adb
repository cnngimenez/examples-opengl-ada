-- load_shaders.adb --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Gl;
with Gl.Objects.Shaders;
with Gl.Files;

with Ada.Text_Io;
use Ada.Text_Io;

function Load_Shaders(Vertex_Path:String; Fragment_Path:String) 
		     return Gl.Objects.Programs.Program is
   
   Shader_Program : Gl.Objects.Programs.Program;
   My_Fshader : Gl.Objects.Shaders.Shader(Gl.Objects.Shaders.Fragment_Shader);
   My_Vshader : Gl.Objects.Shaders.Shader(Gl.Objects.Shaders.Vertex_Shader);
begin
   My_Vshader.Initialize_Id;
   My_Fshader.Initialize_Id;
   Gl.Files.Load_Shader_Source_From_File(My_Vshader, Vertex_Path);       
   Gl.Files.Load_Shader_Source_From_File(My_Fshader, Fragment_Path);
   
   My_Vshader.Compile;
   
   if not My_Vshader.Compile_Status then
      Put_Line("Vertex Shader compilation failed.");
      Put_Line("Log:");
      Put_Line(My_Vshader.Info_Log);
   end if;
   
   My_Fshader.Compile;
   
   if not My_Fshader.Compile_Status then
      Put_Line("Fragment Shader compilation failed.");
      Put_Line("log:");
      Put_Line(My_FShader.Info_Log);
   end if;
   
   Shader_Program.Initialize_Id;
   Shader_Program.Attach(My_Vshader);
   Shader_Program.Attach(My_Fshader);
   Shader_Program.Link;
   
   if not Shader_Program.Link_Status then
      Put_Line("Linking: failed.");
      Put_Line("Log:");
      Put_Line(Gl.Objects.Programs.Info_Log(Shader_Program));
   end if;
   
   Gl.Objects.Shaders.Release_Shader_Compiler;
   
   return Shader_Program;
end Load_Shaders;
