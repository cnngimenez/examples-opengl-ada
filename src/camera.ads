-- camera.ads --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Gl.Types;
use Gl.Types;
with Gl.Objects.Programs;
with Gl.Uniforms;

package Camera is
   
   type Degree is new Single;
   type Radian is new Single;   
   
   -- View
   Camera_Position   : GL.Types.Singles.Vector3 := (2.0, 2.0, 2.0);
   Look_At           : GL.Types.Singles.Vector3 := (0.0, 0.0, 0.0);
   Up                : GL.Types.Singles.Vector3 := (0.0, 1.0, 0.0);
   
   -- Perspective Projection
   View_Width        : Single := 1024.0;
   View_Height       : Single := 768.0;
   View_Angle        : Degree  := 45.0; -- degrees
   Z_Near            : Single := 0.1;
   Z_Far             : Single := 100.0;
   
   -- Matrixs
   MVP_Location        : GL.Uniforms.Uniform;
   MVP_Matrix          : GL.Types.Singles.Matrix4;
   
   -- Movement
   Distance : Single := 2.0;
   Angle_X : Degree := 0.0;
   Angle_Y : Degree := 0.0;
   
   procedure Init_Lookat_Transform
     (Position, Target, Up : Singles.Vector3; Look_At : out Singles.Matrix4);
   procedure Init_Perspective_Transform
     (View_Angle : Degree; Width, Height, Z_Near, Z_Far : Single;
			   Transform  : out Singles.Matrix4);   
   procedure Set_MVP_Matrix (Render_Program : in GL.Objects.Programs.Program);
   
   procedure Move_Up;
   procedure Move_Down;
   procedure Move_Left;
   procedure Move_Right;
   procedure Move_Nearer;
   procedure Move_Farer;
   
end Camera;
