-- particles.ads --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------


with Ada.Numerics.Float_Random;
use Ada.Numerics.Float_Random;

with Gl.Types;
with Gl.Objects.Buffers;
use Gl.Types;
use Gl.Types.Singles;

package Particles is
   
   type Particle is tagged record 
      Position : Vector3;
      Colour : Vector4;
      Life : Gl.Types.Single;
      
      Speed : Gl.Types.Single;
      Y_Step : Gl.Types.Single;
      Y_Amp : Gl.Types.Single;
      
      Xz_Direction : Float;
      X_Step : Gl.Types.Single;
      Z_Step : Gl.Types.Single;
   end record;
   
   procedure Init(Inst : in out Particle; Seed : in Generator);
   procedure Update_Colour(Inst : in out Particle; Seed : in Generator);
   procedure Do_Step(Inst : in out Particle);
   
   -- ** ****************** **
   -- Particle Engine
   -- ** ****************** **
   
   Max_Particles : constant Gl.Types.Int := 10000;
   
   type Particle_Engine is tagged private; 
   
   procedure Init(Inst : in out Particle_Engine);
   procedure Set_Buffers(Inst : in Particle_Engine;
			 Position_Buffer : in out Gl.Objects.Buffers.Buffer;
			 Colour_Buffer : in out Gl.Objects.Buffers.Buffer);
   procedure Update_Buffers(Inst : in Particle_Engine;
			    Position_Buffer : in out Gl.Objects.Buffers.Buffer;
			    Colour_Buffer : in out Gl.Objects.Buffers.Buffer);
   procedure Step(Inst : in out Particle_Engine);
   
private
   Anim_Step : constant Single := 0.01;
   Max_Life : constant Single := 10.0;
   Min_Y_Step : constant Single := -2.0;
   
   type Particle_Arr is array (1..Max_Particles) of Particle;
   
   type Particle_Engine is tagged record
      Particles : Particle_Arr;
   end record;
   
end Particles;
