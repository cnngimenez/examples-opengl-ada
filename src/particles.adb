-- particles.adb --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Ada.Numerics;
with Ada.Numerics.Generic_Elementary_Functions;

with Gl.Objects;

package body Particles is
      
   procedure Init(Inst : in out Particle;
		  Seed : in Generator) is
      use Ada.Numerics;

      -- trigonometrics
      package Trigs is new Generic_Elementary_Functions(Float);
   
   begin
      Inst.Position := (0.0, 0.0, 0.0);
      Inst.Update_Colour(Seed);
      Inst.Life := Single(Random(Seed)) * Max_Life;
      
      Inst.Speed := Single(Random(Seed) * 4.0);
      Inst.Y_Amp := Single(Random(Seed) * 4.0);
      Inst.Y_Step := Min_Y_Step;
      
      Inst.Xz_Direction := Random(Seed) * (Pi * 2.0);      
      Inst.X_Step := Single(Trigs.Cos(Inst.Xz_Direction) * 0.1);
      Inst.Z_Step := Single(Trigs.Sin(Inst.Xz_Direction) * 0.1);
      
      -- Y must be same formulae as in step 
      Inst.Position(Gl.Y) := -Inst.Y_Amp * Inst.Y_Step * Inst.Y_Step;
   end Init;
   
   procedure Update_Colour(Inst : in out Particle;
			   Seed : in Generator) is
      R, G, B : Single;
   begin
      R := Single(Random(Seed));
      G := Single(Random(Seed));
      B := Single(Random(Seed));
      
      Inst.Colour :=  (R, G, B, 0.7);
   end Update_colour;
   
   procedure Do_Step(Inst : in out Particle) is      
      Seed : Generator;
   begin
      if (Inst.Life <= Single(0.0)) then
	 Reset(Seed);
	 Inst.Init(Seed);
      else	 
	 Inst.Position(Gl.Y) := -Inst.Y_Amp * Inst.Y_Step * Inst.Y_Step;
	 Inst.Y_Step := Inst.Y_Step + Inst.Speed * Anim_Step;
	 
	 Inst.Position(Gl.X) := Inst.Position(Gl.X) + Inst.X_Step;
	 Inst.Position(Gl.Z) := Inst.Position(Gl.Z) + Inst.Z_Step;
	 
	 Inst.Life := Inst.Life - 0.1;
      end if;
   end Do_Step;
   
   -- ** ********************
   -- Particle_Engine 
   -- ** ********************
   
   procedure Init(Inst : in out Particle_Engine) is
      Seed : Generator;
   begin
      Reset(Seed);
      
      for I in Inst.Particles'Range loop
	 Inst.Particles(I).Init(Seed);
      end loop;

   end Init;
   
   procedure Update_Buffers(Inst : in Particle_Engine;
			    Position_Buffer : in out Gl.Objects.Buffers.Buffer;
			    Colour_Buffer : in out Gl.Objects.Buffers.Buffer) is 
      
      use Gl.Objects.Buffers; -- Needed for array_buffer
      
      procedure Set_Sub_Data is new
	Gl.Objects.Buffers.Set_Sub_Data(Gl.Types.Singles.Vector3_Pointers);
      
      Position_Data : Vector3_Array(1..Max_Particles);
      -- Colour_Data : Vector4_Array(1..Max_Particles);
      
      procedure Set_vector3_Data is
      begin
	 for I in Inst.Particles'Range loop
	    Position_Data(I) := Inst.Particles(I).Position;
	 end loop;
      end Set_Vector3_Data;

   begin
      Set_Vector3_Data;
      
      Array_Buffer.Bind(Position_Buffer);
      Set_Sub_Data(Array_Buffer, 0, Position_Data);
   end Update_Buffers;
   
   
   procedure Set_Buffers(Inst : in Particle_Engine;
			 Position_Buffer : in out Gl.Objects.Buffers.Buffer;
			 Colour_Buffer : in out Gl.Objects.Buffers.Buffer) is
      
      use Gl.Objects.Buffers; -- Needed for array_buffer      
      
      procedure Load_Vertex_Buffer is new
	Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector4_Pointers);
      procedure Load_Vertex_Buffer is new
	Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector3_Pointers);
      procedure Load_Vertex_Buffer is new
	Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector2_Pointers);
      
      Position_Data : Vector3_Array(1..Max_Particles);
      Colour_Data : Vector4_Array(1..Max_Particles);
      
   begin
      for I in Inst.Particles'Range loop
	 Position_Data(I) := Inst.Particles(I).Position;
	 Colour_Data(I) := Inst.Particles(I).Colour;
      end loop;
      
      Array_Buffer.Bind(Position_Buffer);
      Load_Vertex_Buffer(Array_Buffer, Position_Data, Stream_Draw);
      
      Array_Buffer.Bind(Colour_Buffer);
      Load_Vertex_Buffer(Array_Buffer, Colour_Data, Stream_Draw);
   end Set_Buffers;
   
   procedure Step(Inst : in out Particle_Engine) is
   begin
      for I in Inst.Particles'Range loop
	 Inst.Particles(I).Do_Step;
      end loop;
   end Step;
   
end Particles;
