-- main_particles.adb --- 

-- Copyright 2019 Gimenez Christian
--
-- Author: Gimenez Christian

-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------

with Gl.Buffers;
with Gl.Objects;
with Gl.Objects.Textures;
with Gl.Objects.Textures.Targets;
with Gl.Objects.Buffers;
with Gl.Objects.Vertex_Arrays;
with Gl.Types;
with Gl.Attributes;
with Gl.Uniforms;
with Gl.Blending;
with Gl.Toggles;

-- shaders
with Gl.Objects.Programs;

with Glfw.Windows;
with Glfw.Windows.Context;
with Glfw.Input;
with Glfw.Input.Keys;
with Glfw;

with Ada.Text_Io; 
use Ada.Text_Io;

with Particles;
use Particles;

with Camera;
with Load_Dds;
with Load_Shaders;

procedure Main_Particles is
   
   use Gl.Types;
   use Gl.Objects.Buffers; -- Array_Buffer is here.
   
   Amount_Particles : Gl.Types.Int := Particles.Max_Particles;
   Particles : Particle_Engine;
   
   -- Created with load_shaders():
   Rendering_Program : Gl.Objects.Programs.Program;
   
   Vertex_Buffer : Gl.Objects.Buffers.Buffer;
   Position_Location : Gl.Attributes.Attribute;   
   -- Texture 
   Texture_Id : Gl.Uniforms.Uniform;
   Uvs_Buffer : Gl.Objects.Buffers.Buffer;
   Particle_Texture : Gl.Objects.Textures.Texture;   
   -- Position
   Position_Buffer : Gl.Objects.Buffers.Buffer;
   -- Colours
   Colour_Buffer : Gl.Objects.Buffers.Buffer;
   
   -- Camera
   Mvp_Matrix : Gl.Types.Singles.Matrix4;
   Mvp_Location : Gl.Uniforms.Uniform;
       
   procedure Setup is
      Vertex_Array : Gl.Objects.Vertex_Arrays.Vertex_Array_Object;
      
      procedure Load_Vertex_Buffer is new
	Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector3_Pointers);
      procedure Load_Vertex_Buffer is new
	Gl.Objects.Buffers.Load_To_Buffer(Gl.Types.Singles.Vector2_Pointers);
      
      -- use Gl.Types; -- Singles is here. Also operators overloading of this type.
      
      Vertices : constant Singles.Vector3_Array (1..12) :=
	(
	 -- XY
	 (0.0, 0.0, 0.0),
	 (0.5, 0.0, 0.0),
	 (0.5, 0.5, 0.0),
	 (0.0, 0.5, 0.0),
	 
	 -- XZ
	 (0.0, 0.0, 0.0),
	 (0.0, 0.0, 0.5),
	 (0.5, 0.0, 0.5),
	 (0.5, 0.0, 0.0),
	 
	 -- YZ
	 (0.0, 0.0, 0.0),
	 (0.0, 0.5, 0.0),
	 (0.0, 0.5, 0.5),
	 (0.0, 0.0, 0.5)
	);
      
          
      Uv_Data : constant Singles.Vector2_Array(1..12) :=
	(
	 (0.0, 0.0),
	 (0.0, 1.0),
	 (1.0, 1.0),
	 (1.0, 0.0),
	 
	 (0.0, 0.0),
	 (0.0, 1.0),
	 (1.0, 1.0),
	 (1.0, 0.0),
	   
	 (0.0, 0.0),
	 (0.0, 1.0),
	 (1.0, 1.0),
	 (1.0, 0.0)
	);
            
   begin
      Particles.Init;
      
      Vertex_Array.Initialize_Id;
      Vertex_Array.Bind;
           
      Rendering_Program := Load_Shaders("./shaders/particles_vertex.glsl",
				       "./shaders/particles_texture.glsl");
      
      Position_Location := Gl.Objects.Programs.Attrib_Location(Rendering_Program,
							      "position");
      

      Load_Dds("textures/particle.DDS", Particle_Texture);
      Texture_Id := Gl.Objects.Programs.Uniform_Location(Rendering_Program,
							"myTextureSampler");
      
      Camera.Set_Mvp_Matrix(Rendering_Program);
      
      Vertex_Buffer.Initialize_Id;
      Array_Buffer.Bind(Vertex_Buffer);
      Load_Vertex_Buffer(Array_Buffer, Vertices, Static_Draw);
      
      Uvs_Buffer.Initialize_Id;
      Array_Buffer.Bind(Uvs_Buffer);
      Load_Vertex_Buffer(Array_Buffer, Uv_Data, Static_Draw);
      
      Position_Buffer.Initialize_Id;     
      Colour_Buffer.Initialize_Id;
      Particles.Set_Buffers(Position_Buffer, Colour_Buffer);
      
      Gl.Objects.Textures.Set_Active_Unit(0);
      Gl.Objects.Textures.Targets.Texture_2d.Bind(Particle_Texture);
      -- Gl.Uniforms.Set_Int(Texture_Id, 0);
      Gl.Attributes.Enable_Vertex_Attrib_Array(0);
      Array_Buffer.Bind(Vertex_Buffer);
      Gl.Attributes.Set_Vertex_Attrib_Pointer
	(0, -- index   
	 3, -- size                    
	 Gl.Types.Single_Type, -- type
	 False, -- normalized
	 0, -- stride
	 0 -- pointer
	);
      
      Gl.Attributes.Enable_Vertex_Attrib_Array(1);        
      Array_Buffer.Bind(Uvs_Buffer);
      Gl.Attributes.Set_Vertex_Attrib_Pointer(1, 2,
      					      Gl.Types.Single_Type,
      					      False, 0, 0);
      
      Gl.Attributes.Enable_Vertex_Attrib_Array(2);
      Array_Buffer.Bind(Position_Buffer);
      Gl.Attributes.Set_Vertex_Attrib_Pointer(2, 3,
					      Gl.Types.Single_Type,
					      False, 0, 0);      
      
      Gl.Attributes.Enable_Vertex_Attrib_Array(3);
      Array_Buffer.Bind(Colour_Buffer);
      Gl.Attributes.Set_Vertex_Attrib_Pointer(3, 4,
					      Gl.Types.Single_Type,
					      False, 0, 0);
   end Setup;
   
   
   procedure Render is
      -- use Gl.Types;
   begin
      Rendering_Program.Use_Program;
      
      Gl.Toggles.Enable(Gl.Toggles.Blend);
      Gl.Blending.Set_Blend_Func(Gl.Blending.Src_Alpha,
				 Gl.Blending.One_Minus_Src_Alpha);
      
      
      Gl.Uniforms.Set_Int(Texture_Id, 0);
      
      -- Actualicemos posicion
      Particles.Update_Buffers(Position_Buffer, Colour_Buffer);
      
      
      GL.Attributes.Enable_Vertex_Attrib_Array (0);
      Array_Buffer.Bind (Vertex_Buffer);

      GL.Attributes.Set_Vertex_Attrib_Pointer(0, 3, Single_Type, False, 0, 0);
      GL.Uniforms.Set_Single(Camera.MVP_Location, Camera.MVP_Matrix);

      
      Particles.Step;
      
      Gl.Attributes.Vertex_Attrib_Divisor(0,0);
      Gl.Attributes.Vertex_Attrib_Divisor(1,0);
      Gl.Attributes.Vertex_Attrib_Divisor(2,1);
      Gl.Attributes.Vertex_Attrib_Divisor(3,1);
      Gl.Objects.Vertex_Arrays.Draw_Arrays_Instanced(Gl.Types.Quads, 0, 4, 
						     Amount_Particles);
      -- Gl.Objects.Vertex_Arrays.Draw_Arrays(Gl.Types.Quads, 0, 4);
      
      Gl.Attributes.Disable_Vertex_Attrib_Array(0);
      -- Gl.Attributes.Disable_Vertex_Attrib_Array(1);
   end Render;   
   
   
   
   use Glfw.Input; -- Needed for key_state() below
   
   Main_Window : aliased Glfw.Windows.Window;
   Title : constant String := "Test";
   Running : Boolean := True;
   
   
   procedure Key_Events is
      Cam_Step : Gl.Types.Single := 0.1;
   begin
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_Add)
	= Glfw.Input.Pressed then
	 if (Amount_Particles < Max_Particles) then
	    Amount_Particles := Amount_Particles + 100;
	 end if;
      end if;
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_Substract) 
	= Glfw.Input.Pressed then
	 if (Amount_Particles > 1) then
	    Amount_Particles := Amount_Particles - 100;
	 end if;
      end if;   
      
      -- Camera control
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_8) 
	= Glfw.Input.Pressed then
	 Camera.Move_Up;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_2)
	= Glfw.Input.Pressed then
	 Camera.Move_Down;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_4)
	= Glfw.Input.Pressed then
	 Camera.Move_Left;
	 Camera.Set_Mvp_Matrix(Rendering_Program);	 
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_6)
	= Glfw.Input.Pressed then
	 Camera.Move_Right;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_9)
	= Glfw.Input.Pressed then
	 Camera.Move_Farer;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_3)
	= Glfw.Input.Pressed then
	 Camera.Move_Nearer;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      
      if Main_Window.Key_State(Glfw.Input.Keys.Up) 
	= Glfw.Input.Pressed then
	 Camera.Look_At(Gl.Y) := Camera.Look_At(Gl.Y) + Cam_Step;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Down)
	= Glfw.Input.Pressed then
	 Camera.Look_At(Gl.Y) := Camera.Look_At(Gl.Y) - Cam_Step;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Left)
	= Glfw.Input.Pressed then
	 Camera.Look_At(Gl.X) := Camera.Look_At(Gl.X) - Cam_Step;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   
      if Main_Window.Key_State(Glfw.Input.Keys.Right)
	= Glfw.Input.Pressed then
	 Camera.Look_At(Gl.X) := Camera.Look_At(Gl.X) + Cam_Step;
	 Camera.Set_Mvp_Matrix(Rendering_Program);
      end if;   

      
      if Main_Window.Key_State(Glfw.Input.Keys.Numpad_5)
	= Glfw.Input.Pressed then
	 Put_Line("Amount particles: " & Amount_Particles'Img);
	 Put_Line("Camera at:" 
		    & Camera.Camera_Position(Gl.X)'Img & ", " 
		    & Camera.Camera_Position(Gl.Y)'Img & ", "
		    & Camera.Camera_Position(Gl.Z)'Img 
		 );
	 Put_Line("Look at:" 
		    & Camera.Look_At(Gl.X)'Img & ", " 
		    & Camera.Look_At(Gl.Y)'Img & ", "
		    & Camera.Look_At(Gl.Z)'Img 
		 );
	 Put_Line("Camera Angle and distance:"
		    & Camera.Angle_X'Img & ", "
		    & Camera.Angle_Y'Img & " - "
		    & Camera.Distance'Img);

      end if;   
      
      
   end Key_Events;

begin
   
   Glfw.Init;
   
   Main_Window.Init(1024, 768, Title);
   Glfw.Windows.Context.Make_Current(Main_Window'Access);
   

   Setup;   
   
   while Running loop   
      declare
	 Clearv : Gl.Buffers.Buffer_Bits;
      begin
	 Clearv.Color := True;
	 Clearv.Depth := True;
	 Gl.Buffers.Clear(Clearv);
      end;
            
      Render;
      
      Glfw.Windows.Context.Swap_Buffers(Main_Window'Access);      
      Glfw.Input.Poll_Events;      
      Running := Running and not
	(Main_Window.Key_State(Glfw.Input.Keys.Escape) = Glfw.Input.Pressed);
      Running := Running and not Main_Window.Should_Close;
      
      Key_Events;
      
   end loop;

   Glfw.shutdown;   
   
end Main_Particles;
