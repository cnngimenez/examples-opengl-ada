GLFW_VERSION ?=3

# Copied from OpenGLAda
WINDOWING_BACKEND := windows
GENERATE_EXE := bin/generate.exe
UNAME := $(shell uname)
ifeq ($(UNAME), Darwin)
  WINDOWING_BACKEND := quartz
  GENERATE_EXE := bin/generate
endif
ifeq ($(UNAME), Linux)
  WINDOWING_BACKEND := x11
  GENERATE_EXE := bin/generate 
endif

WINDOWING_SYSTEM := -XWindowing_System=${WINDOWING_BACKEND}
GLFW_VERSION := -XGLFW_Version=${GLFW_VERSION}
PREFIX ?= $HOME/Ada

all: compile

compile:
	gprbuild -p -Pgltest.gpr ${WINDOWING_SYSTEM} ${GLFW_VERSION}

install:
	gprinstall -p -Pgltest.gpr --prefix=${PREFIX}

clean:
	gprclean -Pgltest.gpr

uninstall:
	gprinstall --uninstall -Pgltest.gpr --prefix=${PREFIX}
