# OpenGL Examples an Tests

These are OpenGL examples.

![screenshot](https://bitbucket.org/cnngimenez/examples-opengl-ada/raw/default/screenshot.png) 

Keys defined are at [this line](https://bitbucket.org/cnngimenez/examples-opengl-ada/src/81d07cc57910d6a0d5c094aa76c66d355eeeced1/src/main_particles.adb#lines-240):

As of version [81d07cc](https://bitbucket.org/cnngimenez/examples-opengl-ada/commits/81d07cc57910d6a0d5c094aa76c66d355eeeced1):

- Numpad add and substract control the amount of particles.
- Numpad 8, 2, 6, 4: Move camera position.
- Numpad 9, 3: Move camera nearer and farer.
- Up, down, left, right: Move camera look-at.
- Numpad 5: Display status at stdout.


# Compiling

Requirements:

- GNAT
- GprBuild 
- OpenGLAda

Use gprbuild:

```
gprbuild -XWindowing_System=x11 gltest.gpr
```

# Textures

Textures where extracted from the source code of http://www.opengl-tutorial.org.

