/* particlesf_vertex.glsl 

 Copyright 2019 Gimenez Christian

 Author: Gimenez Christian

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#version 410 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 Vmove;
layout(location = 3) in vec4 pcolour;

out vec2 UV;
out vec4 particle_colour;

uniform mat4 MVP;

void main()
{
    gl_Position = MVP * vec4(position + Vmove , 10.0);
    UV = vertexUV;
    particle_colour = pcolour;
}
