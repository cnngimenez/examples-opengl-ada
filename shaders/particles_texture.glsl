/* particles_texturefragment_shader.glsl 

 Copyright 2019 Gimenez Christian

 Author: Gimenez Christian

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#version 410 core

// Interpolated values from the vertex shaders
in vec2 UV;
in vec4 particle_colour;

out vec4 colour;

// Values that stay constant for the whole mesh.
uniform sampler2D myTextureSampler;

void main()
{
  colour = texture(myTextureSampler, UV) * particle_colour;
  //   colour = vec4(1.0, 0.0 + UV[0] , 0.0 + UV[1], 1.0);
}
